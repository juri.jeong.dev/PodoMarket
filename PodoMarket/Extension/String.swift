//
//  String.swift
//  PodoMarket
//
//  Created by Juri Jeong on 17/10/2022.
//  Copyright © 2022 zool2. All rights reserved.
//

import UIKit

extension String {
    func getStringToDate(strDate: String, format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        guard let date = dateFormatter.date(from: strDate) else { return nil}
        
        return date
    }
    
    func toDate() -> Date? {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMddHHmmss"
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            if let date = dateFormatter.date(from: self) {
                return date
            } else {
                return nil
            }
        }
}
