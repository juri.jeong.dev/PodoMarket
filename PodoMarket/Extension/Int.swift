//
//  Int.swift
//  PodoMarket
//
//  Created by Juri Jeong on 18/10/2022.
//  Copyright © 2022 zool2. All rights reserved.
//

import UIKit

extension Int {
    func numberFormatter(number: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        guard let strNumber = numberFormatter.string(from: NSNumber(value: number)) else { return "" }
        
        return strNumber
    }
}
