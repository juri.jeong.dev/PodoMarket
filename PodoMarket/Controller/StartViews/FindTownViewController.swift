//
//  FindTownViewController.swift
//  PodoMarket
//
//  Created by TJ on 13/08/2019.
//  Copyright © 2019 zool2. All rights reserved.
//
// 연습용 위해 바꾼 것 - recieveTownName ""에 서초동 넣고, buttonGoHome hidden 해제 해둠.

import UIKit
import FirebaseAuth
import Firebase

class FindTownViewController: UIViewController {
    
    @IBOutlet weak var findTownView: UIView!
    @IBOutlet weak var findTownButton: UIButton!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelAskEnter: UILabel!
    @IBOutlet weak var buttonGoHome: UIButton!

    var recieveTownName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        initView()
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            segue.identifier == "webView",
            let destinationVC = segue.destination as? WebViewController
        else { return }
        destinationVC.mainVC = self
    }
        
    func initView() {
        findTownView.layer.cornerRadius = 5
        findTownButton.setTitle("", for: .normal)
        buttonGoHome.layer.cornerRadius = 5
        
        if recieveTownName != nil {
            labelAddress.text = recieveTownName
            labelAddress.isHidden = false
            labelAskEnter.isHidden = false
            buttonGoHome.isHidden = false
        } else {
            labelAddress.isHidden = true
            labelAskEnter.isHidden = true
            buttonGoHome.isHidden = true
        }
    }

    @IBAction func goWebView(_ sender: Any) {
        print("WebView Segue로 View 이동")
    }
    
    @IBAction func goHomeViewTapped(_ sender: Any) {
        guard let recieveTownName = self.recieveTownName else { return }
        saveTownName(town: recieveTownName)
        
        guard let tabBar = storyboard?.instantiateViewController(withIdentifier: "TabbarController") as? UITabBarController else { return }
        self.navigationController?.pushViewController(tabBar, animated: true)
    }
    
    func saveTownName(town: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let loginID = appDelegate.userInfo.loginID
        
        if loginID != nil {
            appDelegate.userInfo.town = town
        } else {
            appDelegate.townSetting.town = town
        }
        
        labelAddress.text = ""
    }
 
}


