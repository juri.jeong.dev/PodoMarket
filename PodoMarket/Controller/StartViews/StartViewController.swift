
import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var btnGotoTownSettings: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {
        btnGotoTownSettings.layer.cornerRadius = 5
    }
  
    @IBAction func pushFindTownVC() {
        // 임시
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.townSetting.town = "서초동"
        
//        guard let homeTabbar = storyboard?.instantiateViewController(withIdentifier: "TabbarController") as? UITabBarController else { return }
//        present(homeTabbar, animated: true)
        
        guard let findTownVC = self.storyboard?.instantiateViewController(withIdentifier: "findTownVC") as? FindTownViewController else { return }
        
        self.navigationController?.pushViewController(findTownVC, animated: true)
    }

}

// https://zeddios.tistory.com/157


