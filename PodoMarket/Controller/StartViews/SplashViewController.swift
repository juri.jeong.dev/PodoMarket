//
//  SplashViewController.swift
//  PodoMarket
//
//  Created by TJ on 24/09/2019.
//  Copyright © 2019 zool2. All rights reserved.
//

import UIKit
import FirebaseRemoteConfig

class SplashViewController: UIViewController {

    var remoteConfig: RemoteConfig!

    override func viewDidLoad() {
        super.viewDidLoad()

        setRemoteConfig()
    }

    func setRemoteConfig() {

        remoteConfig = RemoteConfig.remoteConfig()

        let remoteConfigSettings = RemoteConfigSettings()

        remoteConfig.configSettings = remoteConfigSettings

        // 서버값 받기 (원격 구성 데이터 받아오기, 지속시간 설정)
        remoteConfig.fetch(withExpirationDuration: TimeInterval(5)) { status, error -> Void in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig?.activate()
            } else {
                print("Config not fetched")
                print("Error \(error!.localizedDescription)")
            }
            self.displayRemoteKeyValue()
        }
    }

    func displayRemoteKeyValue() {
        let caps = remoteConfig["splash_message_caps"].boolValue
        let message = remoteConfig["splash_message"].stringValue

        if caps == true {
            let alert = UIAlertController(title: "Checking the server!",
                                          message: message,
                                          preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok",
                                          style: UIAlertAction.Style.default,
                                          handler: { action in
                                              exit(0) })) // 0을 넣으면 앱이 꺼짐
            self.present(alert, animated: true, completion: nil)

        } else {
            guard let startVC = self.storyboard?.instantiateViewController(withIdentifier: "StartVC") as? StartViewController else { return }

            self.navigationController?.pushViewController(startVC, animated: true)
        }
    }

}

// https://studyhard24.tistory.com/52
// https://github.com/firebase/quickstart-ios/tree/master/config
// RemoteConfigDefaults.plist 추가해서 하는 방법
//        let caps = remoteConfig.defaultValue(forKey: "splash_message_caps")!.boolValue
//        let message = remoteConfig.defaultValue(forKey: "splash_message")!.stringValue

// func setRemoteConfig에서
// remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
// 활성화 안해도 실행 됨.

