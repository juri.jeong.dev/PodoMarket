//
//  MyPodoViewController.swift
//  PodoMarket
//
//  Created by 주리 on 8/20/19.
//  Copyright © 2019 zool2. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import SDWebImage

class MyPodoViewController: UIViewController {

    @IBOutlet weak var lableUserLoginStatus: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var revenueView: UIView!
    @IBOutlet weak var labelRevenue: UILabel!
    @IBOutlet weak var salesCompleteView: UIView!
    @IBOutlet weak var wishListView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let db = Firestore.firestore()

    var imageUser: UIImage = UIImage(named: "user.png")!
    var arrayPrice = [Int]()
    var revenue: Int = 0
    var strRevenue: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUserData()
    }
    
    private func initView() {
        self.navigationController?.isNavigationBarHidden = true
        
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        
        revenueView.isHidden = true
        revenueView.layer.cornerRadius = 5
        salesCompleteView.layer.masksToBounds = true
        wishListView.layer.masksToBounds = true
        salesCompleteView.layer.cornerRadius = 5
        wishListView.layer.cornerRadius = 5
    }
    
    func setUserData() {
        if let nickName = appDelegate.userInfo.nickName {
            guard let imageURL = appDelegate.userInfo.imageProfileURL else { return }
            getPrice()
            lableUserLoginStatus.text = "\(nickName)"
            imageView.sd_setImage(with: URL(string: imageURL)) // 초기화면 이미지
            labelRevenue.text = strRevenue
        } else {
            lableUserLoginStatus.text = "Sign in"
            imageView.image = self.imageUser
            contentView.isHidden = true
        }
    }
    
    //FIXME: dataDic의 price 값을 setUserData에 넘겨주는 방법 찾기
    func getPrice() {
        guard let loginID = appDelegate.userInfo.loginID else { return }
        db.collection("Post")
            .whereField("loginID", isEqualTo: loginID)
            .whereField("salesStatus", isEqualTo: "판매완료")
            .getDocuments{ snapshot, error in
                if error != nil {
                    print("Error getting documents: \(String(describing: error))")
                } else {
                    for document in (snapshot?.documents)! {
                        let dataDic = document.data() as NSDictionary
                        guard
                            let price = dataDic["price"] as? String,
                            let intPrice = Int(price)
                        else { return }
                        self.revenue += intPrice
                    }
                    self.strRevenue = Int().numberFormatter(number: self.revenue)
                    print("합계\(self.strRevenue)")
                }
        }
    }
    
    @IBAction func userStatusTapped(_ sender: Any) {
        let loginID = appDelegate.userInfo.loginID
        
        if loginID == nil {
            let LoginVC = storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as? LogInViewController
            self.navigationController?.pushViewController(LoginVC!, animated: true)
        } else {
            let UserProfileVC = storyboard?.instantiateViewController(withIdentifier: "UserProfileVC") as? UserProfileViewController
            self.navigationController?.pushViewController(UserProfileVC!, animated: true)
        }
    }

}
