
import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore

class LogInViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var buttonSignin: UIButton!
    
    var loginID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonLogin.layer.cornerRadius = 5
        buttonSignin.layer.cornerRadius = 5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
        if let loginID = self.loginID {
            textFieldEmail.text = loginID
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    // 터치하면 키보드 사라짐.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.pushHomeVC()
    }
    
    @IBAction func goSignUpTapped(_ sender: Any) {
        
        let signUpVC = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    // MARK:- 포트폴리오 코드
    @IBAction func loginTapped(_ sender: Any) {
        
        let email = textFieldEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = textFieldPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            
            if error != nil {
                if email == "" || password == "" {
                    let alret = UIAlertController(title: "Sign in failed",
                                                  message: error.debugDescription,
                                                  preferredStyle: UIAlertController.Style.alert)
                    print("에러: \(error.debugDescription)")
                    //"The password is invalid or the user does not have a password."
                    
                    let defaultAction = UIAlertAction(title: "Ok",
                                                      style: .destructive,
                                                      handler : nil)
                    
                    alret.addAction(defaultAction)
                    
                    self.present(alret, animated: true, completion: nil)
                } else {
                    let alret = UIAlertController(title: "Sign in failed",
                                                  message: error.debugDescription,
                                                  preferredStyle: UIAlertController.Style.alert)
                    print("에러: \(error.debugDescription)")
                    //"There is no user record corresponding to this identifier. The user may have been deleted."
                    let defaultAction = UIAlertAction(title: "Ok",
                                                      style: .destructive,
                                                      handler : nil)
                    
                    alret.addAction(defaultAction)
                    
                    self.present(alret, animated: true, completion: nil)
                }
            } else {
                let user = Auth.auth().currentUser
                print("\(String(describing: user?.email)), \(String(describing: user?.uid))")
                
                guard let email = user?.email else { return }

                let db = Firestore.firestore()

                db.collection("users")
                    .document("\(email)")
                    .getDocument{ document, error in
                    
                        guard let dataDic = document?.data() as? NSDictionary else { return }
                        let nickname = dataDic["nickname"] as? String ?? ""
                        let imageURL = dataDic["profileImageURL"] as? String ?? ""
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let appDelegateTown = appDelegate.townSetting.town
                            
                        appDelegate.userInfo.loginID = email
                        appDelegate.userInfo.nickName = nickname
                        appDelegate.userInfo.imageProfileURL = imageURL
                        appDelegate.userInfo.town = appDelegateTown
                            
                        self.pushHomeVC()
                }
            }
        }
    }
    
    func pushHomeVC() {
        guard let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as? HomeViewController else { return }
        self.navigationController?.pushViewController(homeVC, animated: true)
    }

}
