import UIKit
import Firebase
import FirebaseDatabase
import FirebaseFirestore
import SDWebImage

class CategorySelectedNSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lableSelectedCategory: UILabel!
    
    let db = Firestore.firestore()
    
    var selectedCategory: String = "" // categoryTab에서 넘어온것
    
    var products = [Product]() // 해당 카테고리에 있는 상품만 넣음
    var currentProducts: [Product] = [] // update table

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
        initView()
        getCategoryPost()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        print("touchesBegan")
    }
    
    func initView() {
        self.navigationController?.isNavigationBarHidden = true
        
        lableSelectedCategory.text = selectedCategory
        
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
        searchBar.placeholder = "Search"
    }
    
    @IBAction func goBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getCategoryPost() {
        
        db.collection("Post")
            .whereField("category", isEqualTo: selectedCategory)
            .whereField("salesStatus", isEqualTo: "")
            .getDocuments{ snapshot, error in
                
            if error != nil {
                print("Error getting documents: \(String(describing: error))")
            } else {
                for document in (snapshot?.documents)! {
                    print("\(document.documentID) => \(document.data())")
                    
                    let dataDic = document.data() as NSDictionary
                    
                    let documentID = document.documentID
                    let title = dataDic["title"] as? String ?? ""
                    let imageUrl1 = dataDic["imageUrl1"] as? String ?? ""
                    let price = dataDic["price"] as? String ?? ""
                    let uploadTime = dataDic["uploadTime"] as? String ?? ""

                    var product = Product()
                    
                    product.imageUrl1 = imageUrl1
                    product.title = title
                    product.price = price
                    product.documentID = documentID
                    product.uploadTime = uploadTime
                
                    self.products.append(product)
                    self.currentProducts = self.products
                }
                self.tableView.reloadData()
            }
        }
    }
    
}

extension CategorySelectedNSearchViewController: UITableViewDelegate, UITableViewDataSource {

    // 셀 높이 설정
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    // 셀 갯수
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return currentProducts.count
    }
    
    // 셀 정보
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as? HomeTableViewCell,
            let imageUrl = currentProducts[indexPath.row].imageUrl1,
            let title = currentProducts[indexPath.row].title,
            let price = currentProducts[indexPath.row].price,
            let intPrice = Int(price),
            let uploadTime = currentProducts[indexPath.row].uploadTime,
            let dateFormat = Date().getStringToDate(strDate: uploadTime,
                                                   format: "yyyyMMddHHmmss")
        else { return UITableViewCell() }
        
        cell.imageProduct.sd_setImage(with: URL(string: imageUrl))
        cell.labelTitle.text = title
        let strPrice = Int().numberFormatter(number: intPrice)
        cell.labelPrice.text = "$\(String(describing: strPrice))"
        let strTimeAgo = Date().timeAgoSince(dateFormat)
        cell.labelUploadTime.text = strTimeAgo
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        guard
            let postVC = storyboard?.instantiateViewController(withIdentifier: "PostViewController") as? PostViewController,
                let documentID = products[indexPath.row].documentID
        else { return }
        postVC.documentID = documentID
        self.navigationController?.pushViewController(postVC, animated: true)
    }
}

extension CategorySelectedNSearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentProducts = products // Put the whole data back into the product
            tableView.reloadData()
            return
        }
    
        // isHaveSearchText가 true인 data만 cureentProducts에 넣음
        currentProducts = products.filter({ products -> Bool in
            guard
                let text = searchBar.text,
                let isHaveSearchText = products.title?.contains(text)
            else { return false }
            return isHaveSearchText
        })
        
        tableView.reloadData()
    }
}
