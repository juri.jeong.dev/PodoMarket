//
//  TabBarController.swift
//  PodoMarket
//
//  Created by Juri Jeong on 27/9/2022.
//  Copyright © 2022 zool2. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectedIndex = 0
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("TabBar \(self.selectedIndex) Selected")
    }
}
