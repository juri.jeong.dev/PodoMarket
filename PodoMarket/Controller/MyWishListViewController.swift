
import UIKit
import FirebaseFirestore
import FirebaseStorage
import SDWebImage
import Toast_Swift

class MyWishListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var labelNotification: UILabel!
    
    let db = Firestore.firestore()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var products: [Product] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }

    override func viewWillAppear(_ animated: Bool) {
        getWishlist()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    private func initView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.addBorder([.top],
                                  color: UIColor.lightGray,
                                  width: 0.5)
    }
    
    @IBAction func btnBackTapped(_ sender: Any) { self.navigationController?.popViewController(animated: true)
    }

    func getWishlist() {
        products.removeAll()
        
        guard let loginID = appDelegate.userInfo.loginID else { return }
        
        db.collection("users")
            .document("\(loginID)")
            .collection("wishlist")
            .getDocuments { snapshot, error in
                
                if error != nil {
                    print("Error getting documents: \(String(describing: error))")
                } else {
                    for document in (snapshot?.documents)! {
                        print("\(document.documentID) => \(document.data())")
                        
                        let wishDocumentID = document.documentID
                        
                        let dataDic = document.data() as NSDictionary
                        
                        guard
                            let documentID = dataDic["documentID"] as? String,
                            let title = dataDic["title"] as? String,
                            let imageUrl1 = dataDic["imageUrl1"] as? String,
                            let category = dataDic["category"] as? String,
                            let price = dataDic["price"] as? String,
                            let uplaodTime = dataDic["uploadTime"] as? String,
                            let dateFormat = Date().getStringToDate(strDate: uplaodTime, format: "yyyyMMddHHmmss")
                        else { return }
                        
                        let strTimeAgo = Date().timeAgoSince(dateFormat)
                        
                        var product = Product()
                        
                        product.wishDocumentID = wishDocumentID
                        product.documentID = documentID
                        product.imageUrl1 = imageUrl1
                        product.title = title
                        product.price = price
                        product.category = category
                        product.uploadTime = strTimeAgo

                        self.products.append(product)
                    }
                    self.checkingPostCount()
                    self.tableView.reloadData()
                }
            }
    }
    
    func checkingPostCount() {
        if self.products.count == 0 {
            self.backgroundView.isHidden = false
        } else {
            self.backgroundView.isHidden = true
        }
    }
    
    func makeToast(message: String) {
        self.view.makeToast(message,
                            duration: 2,
                            point: CGPoint(x: 207, y: 300),
                            title: nil,
                            image: nil,
                            style: .init(),
                            completion: nil)
    }
    
}

extension MyWishListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "wishListCell", for: indexPath) as? HomeTableViewCell else { return UITableViewCell() }
        
        let product = self.products[indexPath.row]
        let productDic = product.getDict()
        
        guard
            let loginID = appDelegate.userInfo.loginID,
            let imageUrl1 = productDic["imageUrl1"] as? String,
            let title = productDic["title"] as? String,
            let price = productDic["price"] as? String,
            let uploadTime = productDic["uploadTime"] as? String,
            let wishDocumentID = productDic["wishDocumentID"] as? String
        else { return UITableViewCell() }
        
        cell.deleteFromList = { [unowned self] in
            db.collection("users")
                .document("\(loginID)")
                .collection("wishlist")
                .document("\(wishDocumentID)")
                .delete() { error in
                    if let error = error {
                        print("Error removing document: \(error)")
                    } else {
                        print("Document successfully removed!")
                        self.getWishlist()
                        self.makeToast(message: "deleted from the Wishlist.")
                    }
            }
        }
        
        let seperate$fromPrice = price.trimmingCharacters(in: ["$"])
        guard let intPrice = Int(seperate$fromPrice) else { return UITableViewCell() }
        let strPrice = Int().numberFormatter(number: intPrice)
        
        cell.imageProduct.sd_setImage(with: URL(string: imageUrl1))
        cell.labelTitle.text = title
        cell.labelPrice.text = strPrice
        cell.labelUploadTime.text = uploadTime
        
        return cell
    }
    
    // 테이블뷰 행 스와이프 삭제
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let product = self.products[indexPath.row]
            // products에 저장된 해당 행의 product 구조체 가져옴.
            // getDic할 필요 없이 바로 "product.wishDocumentID"가져와서 해당 문서 삭제하면 됨.
            
            guard
                let loginID = appDelegate.userInfo.loginID,
                let wishDocId = product.wishDocumentID
            else { return }
            db.collection("users")
                .document("\(loginID)")
                .collection("wishlist")
                .document(wishDocId).delete() { error in
                    if let error = error {
                        print("Error removing document: \(error)")
                    } else {
                        print("Document successfully removed!")
                        self.view.makeToast("Removed from the wishlist",
                                            duration: 2,
                                            point: CGPoint(x: 207, y: 300),
                                            title: nil,
                                            image: nil,
                                            style: .init(),
                                            completion: nil)
                        self.getWishlist()
                    }
            }
        }
    }
    
    // 행 터치
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = self.products[indexPath.row]
        let productDic = product.getDict()
        
        guard
            let documentID = productDic["documentID"] as? String,
            let postVC = storyboard?.instantiateViewController(withIdentifier: "PostViewController") as? PostViewController
        else { return }

        postVC.documentID = documentID

        self.navigationController?.pushViewController(postVC, animated: true)
    }
    
    
}
