//
//  MyPodoMenuNavigationController.swift
//  PodoMarket
//
//  Created by TJ on 18/09/2019.
//  Copyright © 2019 zool2. All rights reserved.
//

import UIKit
import SideMenu

class MyPodoMenuNavigationController: UISideMenuNavigationController {
    
//    원래 사용 했던 SideMenuNavigationController
    let customSideMenuManager = SideMenuManager()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sideMenuManager = customSideMenuManager
        sideMenuManager.menuPresentMode = .viewSlideOutMenuIn
        
        appDelegate.myPodoMenu = self
        
        self.statusBarEndAlpha = 0.0
    }
}
