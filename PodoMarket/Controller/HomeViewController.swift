
import UIKit
import Firebase
import FirebaseFirestore
import FirebaseDatabase
import FirebaseAuth
import SDWebImage

class HomeViewController: UIViewController,UIScrollViewDelegate {
 
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lableNickname: UILabel!
    @IBOutlet weak var lableTown1: UILabel!
    @IBOutlet weak var homeScrollView: UIScrollView!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var lableTown2: UILabel!
    @IBOutlet weak var buttonSignUp: UIButton!
    @IBOutlet weak var buttonLogIn: UIButton!
    
    let db = Firestore.firestore()
    var products = [Product]()

    var imgUser: UIImage = UIImage(named: "user.png")!
    var townName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        loginStatus() // login 상태에 따라 popupView 상태 달라짐
        getProductsData()
    }
    
    @IBAction func goSideMenu(_ sender: Any) {
        guard let sideMenuController = storyboard?.instantiateViewController(withIdentifier: "MyPodoMenuNavigationController") as? MyPodoMenuNavigationController else { return }
        self.present(sideMenuController, animated: true)
    }
    
    @IBAction func popUpBackTapped(_ sender: Any) {
        backgroundView.isHidden = true
        popUpView.isHidden = true
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        backgroundView.isHidden = true
        popUpView.isHidden = true
        let signUpVC = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController
        self.navigationController?.pushViewController(signUpVC!, animated: true)
    }
    
    @IBAction func logInTapped(_ sender: Any) {
        backgroundView.isHidden = true
        popUpView.isHidden = true
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as? LogInViewController
        self.navigationController?.pushViewController(loginVC!, animated: true)
    }
    
    func initView() {
        self.navigationController?.isNavigationBarHidden = true
        
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        
        homeScrollView.delegate = self
        homeTableView.dataSource = self
        homeTableView.delegate = self
        
        topView.layer.addBorder([.bottom], color: UIColor.lightGray, width: 0.5)
    }
    
    func loginStatus() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if let town = appDelegate.userInfo.town {
            noPopupView()
            guard
                let nickname = appDelegate.userInfo.nickName,
                let imageProfileURL = appDelegate.userInfo.imageProfileURL
            else { return }
            
            townName = town
            lableTown1.text = town
            lableTown2.text = town
            
            lableNickname.text = "\(String(describing: nickname))"
            imageView.sd_setImage(with: URL(string: imageProfileURL))
        } else {
            presentPopupView()
            
            let townName = appDelegate.townSetting.town
            self.townName = townName
            lableTown1.text = townName
            lableTown2.text = townName
            
            self.lableNickname.text = "Sign in"
            imageView.image = self.imgUser
        }
    }
    
    func presentPopupView() {
        popUpView.isHidden = false
        backgroundView.isHidden = false

        popUpView.layer.cornerRadius = 10
        popUpView.layer.borderColor = UIColor.lightGray.cgColor
        popUpView.layer.borderWidth = 1
        lableTown2.text = townName
        buttonSignUp.layer.cornerRadius = 10
        buttonLogIn.layer.cornerRadius = 10
    }
    
    func noPopupView() {
        popUpView.isHidden = true
        backgroundView.isHidden = true
    }
    
    func getProductsData() {
        
        products.removeAll()
        
        db.collection("Post")
            .whereField("town", isEqualTo: townName)
            .whereField("salesStatus", isEqualTo: "")
            .getDocuments{ (snapshot, error) in
                if error != nil {
                    print("문서를 가져오는 중 오류 발생: \(String(describing: error))")
                } else {
                    for document in snapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        
                        let documentID = document.documentID
                        let dataDic = document.data() as NSDictionary
                        
                        guard
                            let imageUrl1 = dataDic["imageUrl1"] as? String,
                            let title = dataDic["title"] as? String,
                            let price = dataDic["price"] as? String,
                            let town = dataDic["town"] as? String,
                            let uploadTime = dataDic["uploadTime"] as? String
                        else { return }
                        
                        let uploadTimeInt = self.convertUploadTime(uploadTime: uploadTime)
                        
                        var product = Product()
                        
                        product.imageUrl1 = imageUrl1
                        product.title = title
                        product.price = price
                        product.documentID = documentID
                        product.town = town
                        product.uploadTime = uploadTime
                        product.uploadTimeInt = uploadTimeInt
                        self.products.append(product)
                    }
                    self.products.sort { $0.uploadTimeInt! > $1.uploadTimeInt! } // 최신순 정렬
                    self.homeTableView.reloadData()
                }
            }
        }
    // 최신순 정렬을 위해 Int로 변경함.
    func convertUploadTime(uploadTime: String) -> UInt {
        let stringUploadTime = uploadTime
        let uploadTimeInt: UInt = UInt(stringUploadTime)! // string에서 UInt로 다시 변환
        
        return uploadTimeInt
    }
        
}

extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as? HomeTableViewCell else { return UITableViewCell() }
        
        let product = self.products[indexPath.row]
        let productDic = product.getDict()
        
        guard
            let imageUrl1 = productDic["imageUrl1"] as? String,
            let title = productDic["title"] as? String,
            let price = productDic["price"] as? String,
            let intPrice = Int(price),
            let town = productDic["town"],
            let uploadTime = productDic["uploadTime"] as? String,
            let dateFormat = Date().getStringToDate(strDate: uploadTime,
                                                   format: "yyyyMMddHHmmss")
        else { return UITableViewCell() }
        
        cell.imageProduct.sd_setImage(with: URL(string: imageUrl1))
        cell.labelTitle.text = title
        let strPrice = Int().numberFormatter(number: intPrice)
        cell.labelPrice.text = "$\(strPrice)"
        let strTimeAgo = Date().timeAgoSince(dateFormat)
        cell.labelUploadTime.text = "\(town) · \(strTimeAgo)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let postVC = storyboard?.instantiateViewController(withIdentifier: "PostViewController") as? PostViewController else { return }
        
        let product = self.products[indexPath.row]
        let productDic = product.getDict()
        
        guard let documentID = productDic["documentID"] as? String else { return }
        postVC.documentID = documentID

        self.navigationController?.pushViewController(postVC, animated: true)
    }
}

extension CALayer {
    func addBorder(_ arr_edge: [UIRectEdge], color: UIColor, width: CGFloat) {
        for edge in arr_edge {
            let border = CALayer()
            switch edge {
            case UIRectEdge.top:
                border.frame = CGRect.init(x: 0, y: 0, width: frame.width, height: width)
                break
            case UIRectEdge.bottom:
                border.frame = CGRect.init(x: 0, y: frame.height - width, width: frame.width, height: width)
                break
            case UIRectEdge.left:
                border.frame = CGRect.init(x: 0, y: 0, width: width, height: frame.height)
                break
            case UIRectEdge.right:
                border.frame = CGRect.init(x: frame.width - width, y: 0, width: width, height: frame.height)
                break
            default:
                break
            }
            border.backgroundColor = color.cgColor;
            self.addSublayer(border)
        }
    }
}
//출처: https://devsc.tistory.com/62 [You Know Programing?]
