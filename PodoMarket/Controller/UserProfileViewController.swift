
import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import Toast_Swift

class UserProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var buttonLogout: UIButton!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelNickname: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var buttonModifyProfile: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loginStatus()
    }
    
    private func initView() {
        self.navigationController?.isNavigationBarHidden = true
        //self.tabBarController?.tabBar.isHidden = true
        
        buttonLogout.layer.cornerRadius = 5
        
        imageProfile.layer.cornerRadius = imageProfile.frame.size.width / 2
        imageProfile.layer.borderColor = UIColor.lightGray.cgColor
        imageProfile.layer.borderWidth = 1
        
        buttonModifyProfile.layer.cornerRadius = 5
    }
    
    func loginStatus() {
        guard
            let loginID = appDelegate.userInfo.loginID,
            let nickname = appDelegate.userInfo.nickName,
            let imageURL = appDelegate.userInfo.imageProfileURL
        else { return }
        
        imageProfile.sd_setImage(with: URL(string: imageURL))
        labelNickname.text = "nickname: \(nickname)"
        labelEmail.text = "email: \(loginID)"
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        
        do {
        try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
            self.appDelegate.userInfo.nickName = nil
            self.appDelegate.userInfo.loginID = nil
            self.appDelegate.userInfo.town = nil
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editProfileTapped() {
        print("edit tapped")
        let storyboard: UIStoryboard = self.storyboard!
        let editProfileVC: UIViewController = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as UIViewController
        self.present(editProfileVC, animated: true, completion: nil)
        editProfileVC.modalTransitionStyle = .crossDissolve
    }
    
    @IBAction func deleteAccountTapped(_ sender: Any) {
        guard let user = Auth.auth().currentUser else { return }
        
        let alert = UIAlertController(title: "Are you sure you want to leave?",
                                      message: nil,
                                      preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes",
                                      style: UIAlertAction.Style.default,
                                      handler: { action in
            user.delete { error in
                if error != nil {
                    // 계정 삭제 실패
                } else {
                    self.appDelegate.userInfo.loginID = ""
                    self.view.makeToast("Account deletion is complete.",
                                        duration: 2,
                                        point: CGPoint(x: 207, y: 300),
                                        title: nil,
                                        image: nil,
                                        style: .init(),
                                        completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "No",
                                      style: UIAlertAction.Style.cancel,
                                      handler: { action in
            alert.dismiss(animated: true,
                          completion: nil) }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
