
import UIKit

class CategoryTabViewController: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let categoryNames = [
        "Jewellery", "Phones,Cameras & Computers",
        "Gaming", "Music, TV & Video",
        "Tools,Motor & Hardware","Women Fashion",
        "Man Fashion", "Outdoor & Sports",
        "Health & Beauty","Household & Business",
        "Hobbies & Toys", "Furniture & Interior",
        "Ticket", "Plant",
        "etc"
    ]
    
    let categoryImages: [UIImage] = [
        UIImage(named: "jewellery.png")!, UIImage(named: "digital.png")!,
        UIImage(named: "game.png")!, UIImage(named: "musicTvVideo.png")!,
        UIImage(named: "ToolsMotorHardware.png")!, UIImage(named: "womenFashion.png")!,
        UIImage(named: "manFashion.png")!, UIImage(named: "outdoorSports.png")!,
        UIImage(named: "healthBeauty.png")!, UIImage(named: "householdBusiness.png")!,
        UIImage(named: "collectablesHobbies.png")!, UIImage(named: "furniture.png")!,
        UIImage(named: "ticket.png")!, UIImage(named: "plant.png")!,
        UIImage(named: "etcLogo.png")!
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self
        
        settingCollectionViewLayOut()
    }

}

extension CategoryTabViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func settingCollectionViewLayOut() {
            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            
            layout.sectionInset = UIEdgeInsets(top: 0,left: 16,bottom: 8,right: 16)
            layout.minimumInteritemSpacing = 8
            layout.itemSize = CGSize(width: 170, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? CollectionViewCell else { return UICollectionViewCell() }
        
        cell.imageCategory.layer.cornerRadius = 10
        cell.imageCategory.image = categoryImages[indexPath.item]
        cell.labelCategoryName.text = categoryNames[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedCategory = self.categoryNames[indexPath.row]
        print(selectedCategory)
        
        let categorySearchVC = storyboard?.instantiateViewController(withIdentifier: "categorySearchVC") as? CategorySelectedNSearchViewController
        
        categorySearchVC?.selectedCategory = selectedCategory
        
        self.navigationController?.pushViewController(categorySearchVC!, animated: true)
    }
}
