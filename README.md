# PodoMarket

* YouTube : https://www.youtube.com/watch?v=dnT0JBBDjLA&t=29s

<div>
<img src="https://gitlab.com/juri.jeong.dev/PodoMarket/uploads/1d1313925f13c9bf95f4899a63e0e115/SC_37.png"  width="140" height="340"/>
<img src="https://gitlab.com/juri.jeong.dev/PodoMarket/uploads/67c523d9bc3dcfdffcdc6b7c58726a0c/SC_38.png"  width="140" height="340"/>
<img src="https://gitlab.com/juri.jeong.dev/PodoMarket/uploads/d24c94a1a2433d23574eb3252bb3abe0/SC_39__1_.png"  width="140" height="340"/>
<img src="https://gitlab.com/juri.jeong.dev/PodoMarket/uploads/552009bff186026bd736f05111deedbf/SC_40.png"  width="140" height="340"/>
<img src="https://gitlab.com/juri.jeong.dev/PodoMarket/uploads/86cacfdd4bdc8089aecf6e12d759c79f/SC_41.png"  width="140" height="340"/>
<img src="https://gitlab.com/juri.jeong.dev/PodoMarket/uploads/916382cad0d1c7b932688ac5139705f9/SC_42.png"  width="140" height="340"/>
</div>

# Project Description 
This project is a fan app of Karrot, Korea's No. 1 used trading application.

Karrot is a national application used by all ages in a face-to-face transaction with local people (delivery are also possible).

I used 'Karrot' a lot and it's a affectionate app for me. Because I felt warm while trading with users in the neighborhood.

So I chose to clone the Karrot app as my first iOS project.

# Development period

* 2021.05.01 ~ 2021.08.31 (Under maintenance)
* A personal project


# Skills

* Swift 
* Kakao Post code API 
* Firebase(Remote Config/Authentication/FireStore/Storage)
* Toast-Swift
* SDWebImage
* SideMenu
* TextFieldEffects
* NVActivityIndicatorView
* Git
* Sourcetree
* Figma


# Key Features
YouTube : https://www.youtube.com/watch?v=dnT0JBBDjLA&t=29s

- MVC Architecture
- Setting up a location using the postcode API and WebView
- Sign up and Sign in with Firebase Authentication
- Uploading a Product
- User Page(Edit profile, Manage wishlist)
- Write a comment
- Search within a category
- Create screenshots for app stores using Figma


# Front-End 

<img src="https://gitlab.com/juri.jeong.dev/PodoMarket/uploads/3138397f250c13fa6ab9627c392a9df8/Information_Architecture__FigJam__-_uxchunks.com__Community_.png"/>


# Back-End

<img src="https://gitlab.com/juri.jeong.dev/PodoMarket/uploads/36a59c74421a80bbd8d0a4d94d0da877/Information_Architecture__FigJam__-_uxchunks.com__Community___2_.png"/>


# A problem facing 

This project is the first project and side project to study coding.

So the whole functional implementation process was not easy.

Among them, The chat feature was the one I spent the most time on.

For two weeks, I studied single-tone concepts and proceed a chat function tutorial on YouTube, trying to implement the function. 

However, with my shallow coding knowledge, I couldn't implement it because I didn't know how to apply the code to my project. 

Two weeks of investment in a three-month project is a tremendous amount of time.

So I'm going to make a project about single tone and study it separately. 

Therefore, this project has been changed to Comment instead of Chat.

# Features I want to add
* Apply Skeleton View
* Show Seller's Total Revenue
* Find an ID by email
* Apple Account Association


